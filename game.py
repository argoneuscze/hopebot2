# hopebot2, a Discord bot for roleplaying purposes.
# Copyright (C) 2016 argoneus <argoneuscze@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import yaml

from config import config
from location import Location
from player import Player

CFG_LOCATION_FILE = './settings/game_locations.yaml'
CFG_PLAYER_FILE = './settings/game_players.yaml'


class Game:
    def __init__(self):
        self.locations = {}
        self.players = {}

        self.load_locations()
        self.load_players()

    def add_location(self, channel_name):
        loc = Location()
        self.locations[channel_name] = loc
        self.save_locations()
        return loc

    def add_player(self, user_id):
        p = Player()
        self.players[user_id] = p
        self.save_players()
        return p

    def save_locations(self):
        with open(CFG_LOCATION_FILE, 'w') as outfile:
            yaml.dump(self.locations, outfile, default_flow_style=False)

    def save_players(self):
        with open(CFG_PLAYER_FILE, 'w') as outfile:
            yaml.dump(self.players, outfile, default_flow_style=False)

    def load_locations(self):
        try:
            with open(CFG_LOCATION_FILE, 'r') as stream:
                try:
                    self.locations = yaml.load(stream)
                except yaml.YAMLError as ex:
                    print(ex)
        except FileNotFoundError:
            l = self.add_location(config['default_location'])
            l.locked = False
            l.full_name = 'The Void'
            l.description = 'Nothing here. Just emptiness.'
            self.save_locations()

    def load_players(self):
        try:
            with open(CFG_PLAYER_FILE, 'r') as stream:
                try:
                    self.players = yaml.load(stream)
                except yaml.YAMLError as ex:
                    print(ex)
        except FileNotFoundError:
            pass
