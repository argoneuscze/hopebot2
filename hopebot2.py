# hopebot2, a Discord bot for roleplaying purposes.
# Copyright (C) 2016 argoneus <argoneuscze@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import asyncio

import discord
from discord import Colour
from discord import PermissionOverwrite
from discord import Permissions

from config import config
from game import Game


class HopeBot(discord.Client):
    def __init__(self):
        super().__init__()
        self.game = Game()
        self.server = None
        self.gm_role = None
        self.player_role = None

    @asyncio.coroutine
    def on_ready(self):
        print('Successfully logged in.')
        self.server = self.get_server(config['server_id'])
        yield from self.verify_roles()
        yield from self.verify_channels()

    @asyncio.coroutine
    def verify_roles(self):
        # check for gm and player roles
        for role in self.server.role_hierarchy:
            if role.name == config['gm_role']:
                self.gm_role = role
            elif role.name == config['player_role']:
                self.player_role = role
        # make them if they don't exist
        if not self.gm_role:
            self.gm_role = yield from self.create_role(self.server, name=config['gm_role'])
        if not self.player_role:
            self.player_role = yield from self.create_role(self.server, name=config['player_role'])
        # set correct settings for the roles
        perms = Permissions()
        perms.read_messages = True
        perms.send_messages = True
        perms.read_message_history = True
        yield from self.edit_role(self.server, self.gm_role, colour=Colour.red(), hoist=True,
                                  permissions=perms, name=config['gm_role'])
        yield from self.edit_role(self.server, self.player_role, colour=Colour.teal(), hoist=True,
                                  permissions=perms, name=config['player_role'])
        # give self gm role
        yield from self.add_roles(self.server.get_member(self.user.id), self.gm_role)

    @asyncio.coroutine
    def verify_channels(self):
        # check if all the locations exist
        for ch_name, loc in self.game.locations.items():
            # get channel, make channel if doesn't exist
            try:
                ch = next(c for c in self.server.channels if c.name == ch_name)
            except StopIteration:
                ch = yield from self.create_channel(self.server, ch_name)

            # set permissions
            yield from self.edit_channel_permissions(ch, self.gm_role,
                                                     PermissionOverwrite(read_messages=True, send_messages=True,
                                                                         read_message_history=True))
            yield from self.edit_channel_permissions(ch, self.player_role,
                                                     PermissionOverwrite(read_messages=False, send_messages=False,
                                                                         read_message_history=False))
            yield from self.edit_channel_permissions(ch, self.server.default_role,
                                                     PermissionOverwrite(read_messages=True, send_messages=False,
                                                                         read_message_history=False))
            # set channel topic
            yield from self.edit_channel(ch, topic=loc.description)

    @asyncio.coroutine
    def on_message(self, message):
        # check it's not my own message
        if message.author == self.user:
            return
        # check the person is even in the game
        member = self.server.get_member(message.author.id)
        if not member or not any(x in {self.gm_role, self.player_role} for x in member.roles):
            if message.channel.is_private:
                yield from self.send_message(message.channel, 'You are not part of the game.')
            return

        # handle message
        if message.content.startswith(config['delimiter']):
            cmd, *args = message.content[len(config['delimiter']):].split()
            if self.gm_role in member.roles:
                yield from self.handle_gm_command(member, cmd, args)
            elif self.player_role in member.roles:
                yield from self.handle_player_command(member, cmd, args)

    @asyncio.coroutine
    def handle_gm_command(self, member, cmd, args):
        if cmd == 'lock' and len(args) == 1:
            try:
                loc = self.game.locations[args[0]]
            except KeyError:
                return
            loc.locked = not loc.locked
            self.game.save_locations()
            if loc.locked:
                yield from self.send_message(member, '{} locked.'.format(loc.full_name))
            else:
                yield from self.send_message(member, '{} unlocked.'.format(loc.full_name))
        elif cmd == 'setlocdesc' and len(args) >= 2:
            try:
                loc = self.game.locations[args[0]]
            except KeyError:
                return
            old_desc = loc.description
            loc.description = ' '.join(args[1:])
            self.game.save_locations()
            yield from self.send_message(member,
                                         'Old description: {}\nNew description: {}'.format(old_desc, loc.description))
            yield from self.edit_channel(self.get_location_by_name(args[0])[0], topic=loc.description)
        elif cmd == 'locinfo' and len(args) == 1:
            try:
                loc = self.game.locations[args[0]]
            except KeyError:
                return
            yield from self.send_message(member,
                                         'Channel name: {}\nFull name: {}\nDescription: {}\nLocked: {}'.format(args[0],
                                                                                                               loc.full_name,
                                                                                                               loc.description,
                                                                                                               loc.locked))
        elif cmd == 'playerinfo' and len(args) >= 1:
            try:
                _, target = yield from self.get_player_by_name(' '.join(args))
                yield from self.send_message(member,
                                             'Name: {}\nDescription: {}\nStatus: {}\nLocation: {}'.format(
                                                 target.full_name, target.description, target.status, target.location))
            except LookupError as ex:
                yield from self.send_message(member, ex)
        elif cmd == 'help':
            yield from self.send_message(member,
                                         'Commands: setlocdesc, lock, locinfo, playerinfo, help')
        else:
            yield from self.send_message(member, 'Invalid command. Use {}help for help.'.format(config['delimiter']))

    @asyncio.coroutine
    def handle_player_command(self, member, cmd, args):
        player = yield from self.get_player_by_member(member)
        if cmd == 'move' and len(args) == 1:
            yield from self.move_member_location(member, args[0])
        elif cmd == 'setname' and len(args) >= 1:
            player.full_name = ' '.join(args)
            self.game.save_players()
            yield from self.send_message(member, 'Name changed to {}.'.format(player.full_name))
        elif cmd == 'setdesc' and len(args) >= 1:
            old_desc = player.description
            player.description = ' '.join(args)
            self.game.save_players()
            yield from self.send_message(member,
                                         'Description updated.\nOld description: {}\nNew description: {}'.format(
                                             old_desc,
                                             player.description))
        elif cmd == 'lookat' and len(args) >= 1:
            try:
                _, target = yield from self.get_player_by_name(' '.join(args))
                if target.location != player.location:
                    yield from self.send_message(member, 'You must be in the same location.')
                else:
                    yield from self.send_message(member,
                                                 'Name: {}\nDescription: {}\nStatus: {}'.format(target.full_name,
                                                                                                target.description,
                                                                                                target.status))
            except LookupError as ex:
                yield from self.send_message(member, ex)
        elif cmd == 'setstatus':
            old_status = player.status
            player.status = ' '.join(args)
            yield from self.send_message(member,
                                         'Status updated.\nOld status: {}\nNew status: {}'.format(old_status,
                                                                                                  player.status))
            self.game.save_players()
        elif cmd == 'resetstatus':
            yield from self.send_message(member, 'Status reset to normal.\nOld status: {}'.format(player.status))
            player.status = ''
            self.game.save_players()
        elif cmd == 'selfinfo':
            yield from self.send_message(member,
                                         '= Player info =\nName: {}\nDescription: {}\nStatus: {}\nLocation: {}'.format(
                                             player.full_name, player.description, player.status, player.location))
        elif cmd == 'help':
            yield from self.send_message(member,
                                         'Commands: setname, setdesc, setstatus, resetstatus, selfinfo, move, lookat, help')
        elif cmd == 'force':
            pass
        else:
            yield from self.send_message(member, 'Invalid command. Use {}help for help.'.format(config['delimiter']))

    @asyncio.coroutine
    def move_member_location(self, member, target):
        player = yield from self.get_player_by_member(member)
        # check source location exists, if it doesn't, move to default
        try:
            source, src_loc = self.get_location_by_name(player.location)
        except LookupError:
            player.location = config['default_location']
            self.game.save_players()
            source, src_loc = self.get_location_by_name(player.location)
        # check destination location exists
        try:
            dest, dst_loc = self.get_location_by_name(target)
        except LookupError as ex:
            yield from self.send_message(member, ex)
            return
        # check I'm not already there
        if source == dest:
            yield from self.send_message(member, 'You are already there!')
            return
        # check if it's locked
        if dst_loc.locked or src_loc.locked:
            if dst_loc.locked:
                locked_loc = dst_loc.full_name
            else:
                locked_loc = src_loc.full_name
            # ask to force entry
            yield from self.send_message(member,
                                         '{} is locked! *((Use \'{}force\' to go anyway. Consult with GM))*'.format(
                                             locked_loc, config['delimiter']))
            msg = yield from self.wait_for_message(author=member)
            if msg.content == '{}force'.format(config['delimiter']):
                yield from self.send_message(member, '*Forcing entry.*')
            else:
                yield from self.send_message(member, '*Cancelling forced entry.*')
                return
        # everything's fine, enter location
        yield from self.delete_channel_permissions(source, member)
        # notify people
        yield from self.send_message(source, '{} has left the area.'.format(player.full_name))
        if player.status:
            yield from self.send_message(dest,
                                         '{} has entered the area.\nStatus: {}'.format(player.full_name, player.status))
        else:
            yield from self.send_message(dest, '{} has entered the area.'.format(player.full_name))
        yield from self.edit_channel_permissions(dest, member,
                                                 PermissionOverwrite(read_messages=True, send_messages=True))
        # update player location info
        player.location = target
        self.game.save_players()
        # check status of people in location, send if not empty
        enter_msg = '**Now entering {}...**\nDescription: {}\n= List of people ='.format(dst_loc.full_name,
                                                                                         dst_loc.description)
        for p in self.game.players.values():
            if p != player and p.location == player.location:
                if p.status:
                    enter_msg += '\n{} - {}'.format(p.full_name, p.status)
                else:
                    enter_msg += '\n{}'.format(p.full_name)
        yield from self.send_message(member, enter_msg)

    @asyncio.coroutine
    def get_player_by_id(self, pid):
        member = self.server.get_member(pid)
        if not member:
            raise LookupError('Member does not exist.')
        try:
            player = self.game.players[pid]
        except KeyError:
            player = self.game.add_player(pid)
            yield from self.edit_channel_permissions(self.get_location_by_name(player.location)[0], member,
                                                     PermissionOverwrite(read_messages=True, send_messages=True))
        return member, player

    @asyncio.coroutine
    def get_player_by_name(self, name):
        # check if it matches nickname
        member = self.server.get_member_named(name)
        if not member:
            # check if a name starts with it
            for pid, player in self.game.players.items():
                if player.full_name.lower().startswith(name.lower()):
                    member = self.server.get_member(pid)
                    break
        # check I got a valid player
        if not member or self.player_role not in member.roles:
            raise LookupError('Player not found.')
        player = yield from self.get_player_by_member(member)
        return member, player

    @asyncio.coroutine
    def get_player_by_member(self, member):
        _, player = yield from self.get_player_by_id(member.id)
        return player

    def get_location_by_name(self, name):
        if name not in self.game.locations:
            raise LookupError('Invalid location.')
        for ch in self.server.channels:
            if ch.name == name:
                return ch, self.game.locations[ch.name]
        raise LookupError('Channel not found.')
